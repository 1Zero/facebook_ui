import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_ui/config/palette.dart';
import 'package:flutter_facebook_ui/features/facebook_ui/data/datasources/data.dart';
import 'package:flutter_facebook_ui/features/facebook_ui/data/models/post_model.dart';
import 'package:flutter_facebook_ui/features/facebook_ui/presentation/widgets/widgets.dart';



class HomeScreen extends StatelessWidget {
 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:  CustomScrollView(
        slivers: [
          SliverAppBar(
            backgroundColor: Colors.white,
            title: Text(
              'facebook',
               style:const TextStyle(
                 color:Palette.facebookBlue,
                 fontSize: 28.0,
                 fontWeight: FontWeight.bold,
                 letterSpacing: -1.2,
               ) ,            
            ),
            centerTitle: false,
            floating: true,
            actions: [
              CircleButton(
                icon:Icons.search, 
                iconSize:30.0, 
                onPressed: ()=>print('beta'),
              ),
               CircleButton(
                 icon:Icons.facebook_sharp,
                 iconSize:30.0, 
                 onPressed: ()=>print('search'),
              ),
            ], systemOverlayStyle: SystemUiOverlayStyle.dark,
          ),
          SliverToBoxAdapter(
            child: CreatePostContainer(currentUser: currentUser)
          ),
          SliverPadding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 5.0),
            sliver: SliverToBoxAdapter(
              child: Rooms(onlineUsers: onlineUsers),
            ),
          ),

          SliverPadding(
            padding: const EdgeInsets.fromLTRB(0, 5.0, 0, 5.0),
            sliver: SliverToBoxAdapter(
              child: Stories(  
                currentUser:currentUser,
                stories:stories,
              ),
            ),
          ), 
           SliverList(
              delegate: SliverChildBuilderDelegate(
              (context, index){
                final Post post = posts[index];
                return PostContainer(post:post);
              },
              childCount: posts.length
            )
          )

        ],
      ),
      
    );
  }
}