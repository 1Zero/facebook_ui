import 'package:flutter/material.dart';
import 'package:flutter_facebook_ui/features/facebook_ui/presentation/pages/home_screen.dart';
import 'package:flutter_facebook_ui/features/facebook_ui/presentation/widgets/widgets.dart';

class NavScreen extends StatefulWidget {
  const NavScreen({ Key? key }) : super(key: key);

  @override
  _NavScreenState createState() => _NavScreenState();
}

class _NavScreenState extends State<NavScreen> {
  final List<Widget> _screen =[
    HomeScreen(),
    Scaffold(),
    Scaffold(),
    Scaffold(),
    Scaffold(),
    Scaffold(),

  ];

  final List<IconData> _icons=[

    Icons.home,
    Icons.ondemand_video,
    Icons.account_circle,
    Icons.account_box_outlined,
    Icons.blender_outlined,
    Icons.menu

  ];
  int _selectedIndex =0;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: _icons.length, 
      child: Scaffold(
        body:_screen[_selectedIndex],
        bottomNavigationBar: CustonTabBar(
          icons: _icons,
          selectedIndex:_selectedIndex,
          onTap: (index)=>setState(()=>_selectedIndex = index),
        ),
      )
    );
  }
}