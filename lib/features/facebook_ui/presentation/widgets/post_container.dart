import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_ui/config/palette.dart';
import 'package:flutter_facebook_ui/features/facebook_ui/data/models/post_model.dart';
import 'package:flutter_facebook_ui/features/facebook_ui/presentation/widgets/profile.dart';

class PostContainer extends StatelessWidget {
  final Post post;
  const PostContainer({
    required this.post
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5.0),
      padding: const EdgeInsets.symmetric(vertical: 8.0),      
      color: Colors.white,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,      
              children: [
                _PostHeader(post:post),
                const SizedBox(height: 4.0,),
                Text(post.caption),
                post.imageUrl != null ? const SizedBox.shrink() : const SizedBox(height: 6.0,)
              ],
            ),
          ),
          //se coloca un ! al final del imageUrl: post.imageUrl! para que funcione
          post.imageUrl != null ? 
          CachedNetworkImage(imageUrl: post.imageUrl! ): 
          const SizedBox.shrink(),
          Padding(padding: const EdgeInsets.symmetric(horizontal: 12.0),
                 child: _PostStat(post: post) 
          )
         
        ],
      ),
    );
  }
}

class _PostHeader extends StatelessWidget {
 final Post post;

  const _PostHeader({required this.post});


  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        ProfileAvatar(imageUrl: post.user.imageUrl),
        const SizedBox(width:0.0),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(post.user.name),
  
              Row(children: [
                  Text( 
                    ' ${ post.timeAgo}  *  ',
                    style: TextStyle(color: Colors.grey[600],
                    fontSize:12.0,
                    ),
                  ),
                  Icon(
                    Icons.public,
                    color: Colors.grey[600],
                    size: 12.0,
                  ), 
                ],
              )
            ],
          ),
        ),
        IconButton(
          onPressed: ()=>print('more'), 
          icon: const Icon(Icons.more_horiz
          )
        )

      ],
    );
  }
}

class _PostStat extends StatelessWidget {
  final Post post;

  const _PostStat({required this.post});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(children: [
          Container(
            decoration: BoxDecoration(
              color:Palette.facebookBlue,
              shape: BoxShape.circle, 
            ),
            child: const Icon(
              Icons.thumbs_up_down,
              size: 10.0,
              color: Colors.white,
            ),
          ),
          SizedBox(width: 4.0,),
          Expanded(
            child: Text(
              '${post.likes}',
              style: TextStyle(color: Colors.grey[600]
              ),
            ),
          ),

          Text(
              '${post.comments}comments',
              style: TextStyle(color: Colors.grey[600]),
          ),
          SizedBox(width: 8.0),

          Text(
              '${post.shares}shares',
              style: TextStyle(color: Colors.grey[600]),
          ),
        ],
       ),
       const Divider(),
        Row(
          children: [
            _PostButton(icon: Icon(Icons.thumb_up_alt_outlined), label: 'like', onTab: () => print('like')),
            // _PostButton(
            //   icon: Icons.thumb_up_outlined,
            //   color:Colors.grey[600],
            //   size:20.0,
            //   label:'like',
            //   onTap:() =>print('like'),
            // ),
            _PostButton(icon: Icon(Icons.comment), label: 'comment', onTab: () => print('Comment')),
            _PostButton(icon: Icon(Icons.share), label: 'share', onTab: () => print('share')),
          ],
        ),
      ],
    );
  }
}

class _PostButton extends StatelessWidget {
  final Icon icon;
  final String label;
  final Function? onTab;

  const _PostButton({
    required this.icon, 
    required this.label, 
    required this.onTab
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Material(
        color:Colors.white,
        child: InkWell(
          onTap: null,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 12.0),
            height: 25.0,
            child: Row(children: [
              icon,
              const SizedBox(width: 4.0,),
              Text(label)
            ],
          ),
      
          ),
        ),
      ),
    );
  }
}