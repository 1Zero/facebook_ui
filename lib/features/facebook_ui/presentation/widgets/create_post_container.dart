//import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_ui/features/facebook_ui/data/models/models.dart';
import 'package:flutter_facebook_ui/features/facebook_ui/presentation/widgets/profile.dart';

class CreatePostContainer extends StatelessWidget {
  final User currentUser;

  const CreatePostContainer({ required this.currentUser});

  @override
  Widget build(BuildContext context) {
    return Container(     
      padding: const EdgeInsets.fromLTRB(12.0, 8.0, 12.0, 0.0),
      color: Colors.white,
      child: Column(
        children: [
          Row(
            children: [
              ProfileAvatar(imageUrl: currentUser.imageUrl, isActive: true),
              const SizedBox(width: 8.0,),
               Expanded(
                 child: TextField(
                   decoration: InputDecoration.collapsed(
                     hintText: '¿Que tienes en mente?'
                   ),
                 ),
               ),
            ],
          ),
          const Divider(height: 10.0, thickness: 0.5,),
          Container(
            height: 40.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                TextButton.icon(
                  onPressed: ()=>print('Live'), 
                  icon: const Icon(Icons.videocam,
                  color: Colors.red, 
                 ), 
                  label: Text('Live'),
                ),
                const VerticalDivider(width: 8.0,),
                TextButton.icon(
                  onPressed: () => print('Foto'),
                  icon: const Icon(
                    Icons.photo_library,
                    color: Colors.green,
                  ),
                  label: Text('Fotos'),
                ),
                const VerticalDivider(width: 8.0,
                ),
                TextButton.icon(
                  onPressed: () => print('Room'),
                  icon: const Icon(
                    Icons.video_call,
                    color: Colors.purpleAccent,
                  ),
                  label: Text('Room'),
                ),
                const VerticalDivider(width: 8.0,
                ),
          
              ],
            ),
          ),
        ],
      ),
    );
  }
}